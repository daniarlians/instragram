import React, { Component } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

export default class ContentComp extends Component {
    render() {
        return (
            <View style={style.Image}>
               <Image 
                source={require('../assets/ct.jpg')}
                style={{width:410,height:600,marginTop:5,}}
               />
                <Image 
                source={require('../assets/33.jpg')}
                style={{width:450,height:800,marginTop:5,}}
               />
                <Image 
                source={require('../assets/44.jpg')}
                style={{width:410,height:710,marginTop:5,}}
               />
                <Image 
                source={require('../assets/55.jpg')}
                style={{width:410,height:700,marginTop:5,}}
               />
                <Image 
                source={require('../assets/22.jpg')}
                style={{width:495,height:800,marginTop:5,}}
               />

            </View>
        )
    }
}

const style = StyleSheet. create({
    Image: {
        height:3600,
        alignContent:'center',
        justifyContent:'space-between'
    }
})