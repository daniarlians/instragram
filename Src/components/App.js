import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import {Header} from './index';
import {Story} from './index';
import {Conten} from './index';
import Footer from './Footer'; 

const App = () => {
    return (
        <View>
            <Header />
            <ScrollView>
                <Story />
            </ScrollView>
            <ScrollView>
                <Conten />
            </ScrollView>
                <Footer />
        </View>

        
    )
}

export default App
