import React from 'react';
import {View, Text, ScrollView, Image,} from  'react-native';

const Story =() => {
    return (
        <View >
            <ScrollView horizontal={true}
            showsHorizontalScrollIndicator={false} style={{height:110,backgroundColor: 'white',paddingHorizontal:15}}>
                <Image source={require('../assets/cb.jpg')} 
                style={{height: 75,width: 100,borderRadius:60,marginTop:20,marginHorizontal:8}} />
                <Image source={require('../assets/b.jpg')} 
                style={{height: 70,width: 70,borderRadius:60,marginTop:20,marginHorizontal:1}} />
                <Image source={require('../assets/c.jpg')} 
                style={{height: 70,width: 70,borderRadius:60, marginTop:20,marginHorizontal:15}} />
                <Image source={require('../assets/f.jpg')} 
                style={{ height: 70,width: 70, borderRadius:60,marginTop:20,marginHorizontal:1}} />
                <Image source={require('../assets/g.jpg')} 
                style={{height: 70,width: 70,borderRadius:60, marginTop:20,marginHorizontal:13}} />
                <Image source={require('../assets/h.jpg')} 
                style={{height: 70,width: 70, borderRadius:60, marginTop:20,marginHorizontal:8}} />
                <Image source={require('../assets/a.jpg')} 
                style={{height: 70,width: 70, borderRadius:60, marginTop:20,marginHorizontal:7}} />
          </ScrollView>
        </View>
    )
}

export default Story